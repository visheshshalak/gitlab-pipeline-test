FROM node:14.15.4-alpine
# Forcing to use linux platform only for mac m1 chip

WORKDIR /app

COPY ./package-lock.json ./
COPY ./package.json ./

RUN npm ci --legacy-peer-deps

COPY  ./dist ./

CMD ["node", "src/main.js"]
